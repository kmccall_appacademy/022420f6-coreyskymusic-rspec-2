def measure(num = 1)
  initial_time = Time.now
  num.times { yield }
  (Time.now - initial_time) / num
end
